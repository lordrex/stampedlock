package example.counters;

import javafx.util.Pair;

import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

public class ReentrantLockCounter extends DatedCounter {

    private final ReentrantLock lock = new ReentrantLock();

    public long increment() {
        lock.lock();
        try {
            counter++;
            counterDate = new Date();
            return counter;
        } finally {
            lock.unlock();
        }
    }

    public Pair<Long, Date> getValue() {
        lock.lock();
        try {
            return new Pair(counter, counterDate);
        } finally {
            lock.unlock();
        }
    }

}
