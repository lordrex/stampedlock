package example.counters;

import javafx.util.Pair;

import java.util.Date;

public class OptimisticStampedLockCounter extends DatedCounter {

    public long increment() {
        // TODO: increment counter and set counter date
        // return new counter value
        // use StampedLock to synchronize
        return 0;
    }

    public Pair<Long, Date> getValue() {
        // TODO: return pair - counter and counter date
        // use optimistic lock from StampedLock to synchronize
        return null;
    }

}
