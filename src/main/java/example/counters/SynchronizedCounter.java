package example.counters;

import javafx.util.Pair;

import java.util.Date;

public class SynchronizedCounter extends DatedCounter {

    public synchronized long increment() {
        counter++;
        counterDate = new Date();
        return counter;
    }

    public synchronized Pair<Long, Date> getValue() {
        return new Pair<>(counter, counterDate);
    }

}
