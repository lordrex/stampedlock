package example.counters;

import javafx.util.Pair;

import java.util.Date;

public abstract class DatedCounter {

    protected volatile long counter = 0;
    protected volatile Date counterDate;

    public abstract long increment();

    public abstract Pair<Long, Date> getValue();

}
