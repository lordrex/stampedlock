package example.counters;

import javafx.util.Pair;

import java.util.Date;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ReentrantReadWriteCounter extends DatedCounter {

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public long increment() {
        lock.writeLock().lock();
        try {
            counter++;
            counterDate = new Date();
            return counter;
        } finally {
            lock.writeLock().unlock();
        }
    }

    public Pair<Long, Date> getValue() {
        lock.readLock().lock();
        try {
            return new Pair<>(counter, counterDate);
        } finally {
            lock.readLock().unlock();
        }
    }

}
