package example;

import example.counters.*;

/**
 * Please, implement missing logic in StampedLockCounter and OptimisticStampedLockCounter.
 */
public class Main {

    private final static int  READ_THREAD_COUNT = 3;
    private final static long TEST_EXECUTION_TIME_MS = 3000;
    private final static int  TEST_ITERATIONS_COUNT = 5;

    public static void main(String[] args) throws InterruptedException
    {
        SimpleBenchmark simpleBenchmark = new SimpleBenchmark(READ_THREAD_COUNT, TEST_ITERATIONS_COUNT, TEST_EXECUTION_TIME_MS);
        simpleBenchmark.testImplementation( new SynchronizedCounter() );
        simpleBenchmark.testImplementation( new ReentrantLockCounter() );
        simpleBenchmark.testImplementation( new ReentrantReadWriteCounter() );
        simpleBenchmark.testImplementation( new StampedLockCounter() );
        simpleBenchmark.testImplementation( new OptimisticStampedLockCounter() );
    }



}
