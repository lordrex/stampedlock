package example;

import example.counters.DatedCounter;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.LongStream;

public class SimpleBenchmark {

    private final int readingThreadsCount;
    private final int testIterationsCount;
    private final long executionTimeMs;

    public SimpleBenchmark(int readingThreadsCount, int testIterationsCount, long executionTimeMs) {
        this.readingThreadsCount = readingThreadsCount;
        this.testIterationsCount = testIterationsCount;
        this.executionTimeMs = executionTimeMs;
    }

    public void testImplementation( DatedCounter datedCounter) throws InterruptedException {
        System.out.println("Testing " + datedCounter.getClass().getSimpleName() + ", please wait..." );

        double average = LongStream.rangeClosed( 1, testIterationsCount )
                .map( value -> {
                    long readsPerMs = testReadsPerMsCount(datedCounter);
                    System.out.println( value + "/" + testIterationsCount + " reads per ms = " + readsPerMs );
                    return readsPerMs;
                } )
                .average()
                .getAsDouble();

        System.out.println( "Reads per ms = " + average );
    }

    private long testReadsPerMsCount(DatedCounter counter )
    {
        ExecutorService executorService = Executors.newFixedThreadPool( readingThreadsCount + 1 );
        long startTime = System.currentTimeMillis();

        startWritingThread(counter, executorService);
        List<AtomicLong> readsCounters = startReadingThreads( counter, executorService );
        stopThreadsAfterSpecifiedTime(executorService);

        long readsCount = readsCounters.stream().mapToLong( readCounter -> readCounter.get() ).sum();
        return readsCount / getExecutionTime( startTime );
    }

    private List<AtomicLong> startReadingThreads(DatedCounter datedCounter, ExecutorService executorService ) {
        List<AtomicLong> readsCounters = new LinkedList<>();

        LongStream.rangeClosed( 1, readingThreadsCount )
                .forEach( value -> {
                    AtomicLong readsCount = new AtomicLong();
                    readsCounters.add( readsCount );
                    executorService.submit( () -> {
                        while (!Thread.interrupted()) {
                            datedCounter.increment();
                            readsCount.incrementAndGet();
                        }
                    } );
                } );

        return readsCounters;
    }

    private void startWritingThread(DatedCounter datedCounter, ExecutorService executorService ) {
        executorService.submit( () -> {
            while ( !Thread.interrupted() ) {
                datedCounter.increment();
//                LockSupport.parkNanos( 1000 );
            }
        } );
    }

    private void stopThreadsAfterSpecifiedTime( ExecutorService executorService ) {
        try {
            Thread.sleep( executionTimeMs );
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        executorService.shutdownNow();
    }

    private long getExecutionTime( long startTime ) {
        return System.currentTimeMillis() - startTime;
    }

}
